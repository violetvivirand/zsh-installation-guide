# Zsh Installation Guide

[Zsh](https://zh.wikipedia.org/zh-tw/Z_shell) 是一個功能強大的 Shell，幾乎相容所有於 Bash 執行的腳本，且擁有眾多的快捷操作、自動補完功能，再搭配 [Oh My Zsh 框架](http://ohmyz.sh)，可以幫 Zsh 加入多種外掛、佈景主題的支援，讓使用者在操作系統時更加順手。

## 安裝 Zsh

Zsh 幾乎都可以透過套件管理系統來安裝，具體的安裝流程可以參考 [oh-my-zsh Wiki 中的操作說明](https://github.com/robbyrussell/oh-my-zsh/wiki/Installing-ZSH)。

若要馬上將 Zsh 設為系統預設的 Shell，可以輸入以下指令來設定：

```bash
$ chsh -s /bin/zsh
```

也可以等到下個步驟安裝完 Oh My Zsh 之後，互動式介面會直接提醒並將 Zsh 設為預設的 Shell。

### 將原本 Bash Shell 底下的環境變數加入 Zsh 環境

將 `~/.zshrc` 中 `export PATH=` 該段的註解給去掉。

## 安裝 Oh My Zsh

Oh My Zsh 官方有提供安裝腳本，可以參考[官方的說明](https://github.com/robbyrussell/oh-my-zsh#basic-installation)，搭配 `curl` 或 `wget` 指令安裝 Oh My Zsh。

之後所有與 Zsh 相關的設定值皆會置於 `~/.zhsrc` 檔案之中。如果要看到 Zsh Shell 在啟動時會讀取的腳本和順序，可以執行 `man zsh` 並閱讀 **FILES** 章節。

## 安裝終端機應用程式的 Color Theme

為了讓介面美觀、也便於搭配之後的 Oh My Zsh 佈景主題，可以先幫自己操作得順手的 Terminal 終端機應用程式安裝色彩佈景主題 Color Theme。

較常見使用的色彩佈景主題有 [Solarized](http://ethanschoonover.com/solarized)，對許多的應用程式皆有支援。

### Mac OS: iTerm2

Mac OS 底下推薦搭配使用的終端機應用程式為 iTerm2。

* iTerm2 官方網站：[https://www.iterm2.com]()
* Iterm2-color-schemes：[http://iterm2colorschemes.com]()

之後針對使用的 Profile 檔來個別套用 Color Theme。

### Windows: PuTTY / PieTTY 

Windows 底下推薦的終端機應用程式通常為 PuTTY 的進化版：PieTTY。

> 至於 PuTTY 的 Color Theme 在 PieTTY 上是否可以沿用則需要再確認。

* PuTTY 官方網站：[https://www.chiark.greenend.org.uk/~sgtatham/putty/]()
* PieTTY 官方網站：[https://sites.google.com/view/pietty-project]()
* PuTTY 色彩佈景主題集合 (GitHub)：[https://github.com/AlexAkulov/putty-color-themes]()

## Oh My Zsh Plugin

Oh My Zsh 安裝時即內建許多外掛程式 Plugin，也可以自行下載新的 Plugin 來使用。

### 啟用 Plugin

內建 Plugin 列表以及啟用的方式請參考 [Oh My Zsh 的 Wiki](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins)，也就是修改 `~/.zshrc` 檔案中的 `plugins=` 設定值，以括號將所有 Plugin 包含在內，而不同的 Plugin 之間以空白（或換行符號）隔開。例：

```
# This way
plugins=(rails git ruby)

# or this way
plugins=(
  git
  bundler
  dotenv
  osx
  rake
  rbenv
  ruby
)
```

### Plugin recommended: zsh-autosuggestions

一般會推薦至少多安裝這個 zsh-autosuggestions Plugin，是個會依據先前輸入的指令，再下次輸入時預先給予建議的 Ｐlugin。

安裝方式請參考[官方文件](https://github.com/zsh-users/zsh-autosuggestions#installation)，推薦使用[搭配 Oh My Zsh 的方案](https://github.com/zsh-users/zsh-autosuggestions#oh-my-zsh)來安裝。

## Oh My Zsh Theme

Oh My Zsh 會幫 Zsh 加上更多可以顯示的資訊。安裝 Oh My Zsh 時就已經內建了許多性能優異的 Theme，也可以再額外安裝其他的 Theme。

### Official Theme: agnoster, ys

如果要使用內建的 Theme，比較推薦的有 agnoster 和 ys，搭配 Dark 系列的 Color Theme 應該都可以運作得相當順利。

* angoster
	* Oh My Zsh 官方 Wiki 介紹：[https://github.com/robbyrussell/oh-my-zsh/wiki/themes#agnoster]()
	* agnoster GitHub Repository：[https://github.com/agnoster/agnoster-zsh-theme]()
* ys
	* Oh My Zsh 官方 Wiki 介紹：：[https://github.com/robbyrussell/oh-my-zsh/wiki/themes#ys]()
	* 作者 Blog 介紹：[http://blog.ysmood.org/my-ys-terminal-theme/]()

### Unofficial Theme: Powerlevel9k

Powerlevel9k 是個功能相當強大的 Zsh Theme。[官方的安裝說明](https://github.com/bhilburn/powerlevel9k/wiki/Install-Instructions#option-2-install-for-oh-my-zsh)稍嫌繁瑣，推薦的安裝過程如下：

#### 安裝 Powerlevel9k Theme

安裝 Powerlevel9k Theme](https://github.com/bhilburn/powerlevel9k/wiki/Install-Instructions#step-1-install-powerlevel9k) 時推薦使用[搭配 Oh My Zsh 的安裝方式](https://github.com/bhilburn/powerlevel9k/wiki/Install-Instructions#option-2-install-for-oh-my-zsh)。

之後修改 `~/.zshrc` 啟用 Theme：

```
# Powerlevel9k Configurations
# [NOTE] 這部分一定要放在檔案的最前面

# 套用 Nerd-fonts
POWERLEVEL9K_MODE='nerdfont-complete'

# 換行設定
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_RPROMPT_ON_NEWLINE=true

# 命令左側提示元素
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon context dir_writable dir vcs newline virtualenv nodeenv rbenv)

# 命令右側提示元素
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs history time)

# Python Virtualenv 提示顏色設定
POWERLEVEL9K_VIRTUALENV_FOREGROUND='220'
POWERLEVEL9K_VIRTUALENV_BACKGROUND='000'

# 啟用 Theme
ZSH_THEME="powerlevel9k/powerlevel9k"
```

除了啟用 Theme 的方法為固定以外，其餘設定並沒有標準，可以參考 [Powerlevel9k 官方提供的設定方法](https://github.com/bhilburn/powerlevel9k#prompt-customization)來客製化，或是參考[其他使用者的設定](https://github.com/bhilburn/powerlevel9k/wiki/Show-Off-Your-Config)。

#### 安裝 Powerline Font 字型

在 Powerlevel9k 佈景主題之下，時常會使用到一些特殊字元或是圖示，需要用一些手法將他們整合在同一套字型內比較好管理，於是有了這些 Patch 的方法和 Pre-Patched Fonts 的方案來[安裝 Powerline Font 字型](https://github.com/bhilburn/powerlevel9k/wiki/Install-Instructions#step-2-install-a-powerline-font)。

推薦使用 [Nerd-Fonts 的方案](https://github.com/bhilburn/powerlevel9k/wiki/Install-Instructions#option-4-install-nerd-fonts)，而這裡推薦套用其中的 Knack 或 Meslo 系列字型。

之後套用至終端機應用程式。

##### Mac OS 搭配 Homebrew 安裝

透過 [Homebrew cask](https://caskroom.github.io) 可以安裝一些 Homebrew 提供的 [nerd-font](https://github.com/caskroom/homebrew-fonts/tree/master/Casks)，在列表中搜尋 `nerd-font` 可以找到支援的 Nerd Fonts，接著輸入：

```bash
# 第一次才需要 tap
$ brew tap caskroom/fonts

# 安裝 nerd-font 字型
$ brew cask install font-hack-nerd-font
```

##### 手動安裝

從 Patched Fonts 列表中選擇喜歡的下載，之後匯入各個系統的字型庫。

##### Clone Repo 再安裝（僅適用於 Linux & Mac OS）

先使用指令 `git clone` 將整個 Repo 下載下來，之後搭配 `install.sh` 腳本安裝字型。

```bash
# Clone Repo
$ git clone https://github.com/ryanoasis/nerd-fonts.git
$ cd nerd-fonts

# Install All Nerd-fonts
./install.sh

# Install Specific Nerd-font
$ ./install.sh <FontName>
$ ./install.sh Hack
$ ./install.sh HeavyData
```
